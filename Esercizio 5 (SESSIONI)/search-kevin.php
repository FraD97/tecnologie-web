<?php
    include("top.php");
    include_once("functions.php");
    ensure_log_in();
    $rows = search_kevin(); 
    
?>

<h1>Results for <?= $_GET['firstname']." ".$_GET['lastname']?> with Kevin Bacon</h1>

<?php
    print_table($rows);
    include 'bottom.html';
?>

