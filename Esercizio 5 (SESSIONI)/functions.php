<?php

    function is_login_correct($name, $password){
         try{    
            $db = new PDO("mysql:dbname=imdb_small;host=localhost", "root", "");
            $name = $db->quote($name);
            $password = $db->quote($password);
            $query = "SELECT name FROM user WHERE name =" .$name ."and password =".$password;
            $row = $db->query($query);
            $user_name = $row->fetch(PDO::FETCH_ASSOC);
            return $user_name['name'];
            
        } catch (PDOException $ex) {
            ?>
            <p>Sorry, a database error occurred.</p>
            <?php
            return NULL;
        }
        
    }
    
    
    function find_correct_id($first_name, $last_name, $db){
         $first_name = $first_name . '%';
         $first_name = $db->quote($first_name);
         $last_name = $db->quote($last_name);
         $query = "select min(X.id) as id
                   from(select *
                        from actors
                        where first_name like" . $first_name . "and last_name =" . $last_name . "and film_count = 
                                                                                    (select max(film_count)
                                                                                     from actors
                                                                                     where first_name like".$first_name.
                                                                                     "and last_name = ".$last_name."))X;";
         $id_row = $db->query($query);
         $id = $id_row->fetch(PDO::FETCH_ASSOC);
         return $id['id'];
    }
    
    
    function search_all_actors(){
        try{ 
            
            $first_name = $_GET['firstname'];
            $last_name = $_GET['lastname']; 
            $db = new PDO("mysql:dbname=imdb_small;host=localhost", "root", "");
            $id = find_correct_id($first_name, $last_name, $db);
            $id = $db->quote($id);
            $query = "select name, year from
                     (select * from actors WHERE id =".$id.")X JOIN roles ON X.id = 
                      roles.actor_id JOIN movies ON movies.id = roles.movie_id ORDER BY year DESC, name ASC";
            $rows = $db->query($query);
            return $rows;
            
        } catch (PDOException $ex) {
            ?>
            <p>Sorry, a database error occurred.</p>
            <?php
            return NULL;
        }
    }
    
    function search_kevin(){
        try{     
            $first_name = $_GET['firstname'];
            $last_name = $_GET['lastname']; 
            $db = new PDO("mysql:dbname=imdb_small;host=localhost", "root", "");
            $id = find_correct_id($first_name, $last_name, $db);
            $id = $db->quote($id);
            $query = "SELECT A1.MOVIE_NAME as name, A1.MOVIE_YEAR as year
                      FROM(
                        SELECT A.id as ACTOR_ID, M.id as MOVIE_ID, M.name as MOVIE_NAME, M.year as MOVIE_YEAR
                        FROM actors A join roles R on A.id = R.actor_id  JOIN movies M on R.movie_id = M.id
                        WHERE first_name = 'kevin' and last_name = 'Bacon')A1 
                        JOIN(
                        SELECT M.id as MOVIE_ID
                        FROM actors A join roles R on A.id = R.actor_id  JOIN movies M on R.movie_id = M.id
                        WHERE A.id =".$id.")A2
                        ON A1.MOVIE_ID = A2.MOVIE_ID ORDER BY year DESC, name ASC";
            $rows = $db->query($query);
            return $rows;
        } catch (PDOException $ex) {
            ?>
            <p>Sorry, a database error occurred.</p>
            <?php
            return NULL;
        }      
    }


    function print_table($rows){
        if($rows != null && $rows->rowCount() != 0){
            $i = 1;
    ?>      
        <div id = "table_submit">
                <h4>All Films</h4>
                  <table>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Year</th>
                        </tr>
        <?php        
                foreach($rows as $row){
        ?>          
                    <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $row['name']?></td>
                        <td><?= $row['year']?></td>
                    </tr>
        <?php        
                }
        ?>
                </table>  
            </div>
        <?php
            }
            else{
        ?>
                <p>Sorry no film found</p>
        <?php    

            }

        }


    function redirect($url, $flash_message = NULL) {
      if ($flash_message) {
        $_SESSION["flash"] = $flash_message;
      }
      # session_write_close();
      header("Location: $url");
      die;
    }

    function ensure_log_in(){
        if (!isset($_SESSION["user_name"])) {
            redirect("index.php", "You must log in before you can view that page."); 
        }
    }

