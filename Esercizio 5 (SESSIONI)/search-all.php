<?php
    include("top.php");
    include_once("functions.php");
    ensure_log_in();
    $rows = search_all_actors();
    
?>
    <h1>Results for <?= $_GET['firstname']." ".$_GET['lastname']?></h1>

<?php
    print_table($rows);
    include 'bottom.html';
?>
