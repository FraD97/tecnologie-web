<?php session_start();
    include'functions.php';?>
<!DOCTYPE html>
<html>
	<!-- MFN0634 TWeb Lab05 (Kevin Bacon) -->
	<head>
		<title>My Movie Database (MyMDb)</title>
		<meta charset="utf-8" >
		
		<!-- Links to provided files.  Do not edit or remove these links -->
		<link href="http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/5/favicon.png" type="image/png" rel="shortcut icon" >
		<script src="http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/5/provided.js" type="text/javascript"></script>

		<!-- Link to your CSS file that you should edit -->
		<link rel="stylesheet" type="text/css" href="bacon.css">
	</head>
<body>
    <form id="login" action="login.php" method="post">
        <?php 
            if(isset($_SESSION['flash'])){?>
            <p><?=$_SESSION['flash']?></p>
        <?php
            unset($_SESSION['flash']);
            }?>
        <dl>
            <dt>Name</dt>     <dd><input type="text" name="user_name" /></dd>
            <dt>Password</dt> <dd><input type="password" name="password" /></dd>
            <dt></dt>         <dd><input type="submit" value="Log in" /></dd>
        </dl>
  </form>
</body>

</html>