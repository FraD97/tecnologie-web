CREATE TABLE user (
    ID INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL UNIQUE,
    password VARCHAR(8) NOT NULL
);

INSERT INTO user (name, password) VALUES ("Francesco", "ciao");
INSERT INTO user (name, password) VALUES ("Giuseppe", "come");
INSERT INTO user (name, password) VALUES ("Daniele", "va");