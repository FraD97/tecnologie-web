<?php session_start(); ?>

<!DOCTYPE html>
<html>
	<!-- MFN0634 TWeb Lab05 (Kevin Bacon) -->
	<head>
		<title>My Movie Database (MyMDb)</title>
		<meta charset="utf-8" >
		
		<!-- Links to provided files.  Do not edit or remove these links -->
		<link href="http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/5/favicon.png" type="image/png" rel="shortcut icon" >
		<script src="http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/5/provided.js" type="text/javascript"></script>

		<!-- Link to your CSS file that you should edit -->
		<link rel="stylesheet" type="text/css" href="bacon.css">
	</head>

	<body>
		<div id="frame">
			<div id="banner">
                <div>
                    <a href="index.php"><img src="http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/5/mymdb.png" alt="banner logo"></a>
                                    <span id = "title">My Movie Database</span>
                </div>
                <div id = "log_panel">
                    <?php if(isset($_SESSION['user_name'])){ ?>
                             <a href = "logout.php"><input type="button" value="LOGOUT"></a>
                    <?php }
                          else{ ?>
                             <a href = "user.php"><input type="button" value="LOGIN"></a>
                    <?php    } ?>
                </div>
                
			</div>

			<div id="main">
