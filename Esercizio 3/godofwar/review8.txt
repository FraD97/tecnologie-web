Despite the non-numerical title suggesting a complete do-over, God of War is thrillingly different. This is more redemption than reinvention, though there is plenty of that too, as Sony Santa Monica levy the weight of Kratos� past in one of the most gorgeous, spectacular and impactful blockbusters of the generation.
FRESH
Roger Ebert
Telegraph