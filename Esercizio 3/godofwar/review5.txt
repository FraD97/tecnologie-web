The God of War series has undergone a fantastic transformation. I'll miss the fixed camera and tales of revenge that previous titles provided but this new evolution makes for a far superior and profound experience. It's one of the best games of this generation.
FRESH
Bryant Frazer
Video Chums