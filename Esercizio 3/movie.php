<?php

    $film=$_GET["film"];
      
 ?>

<!DOCTYPE html>
<html>
	<head>
		<title>TMNT - Rancid Tomatoes</title>
        <link href="http://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/rotten.gif" type= "image/gif" rel = "shortcut icon">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="movie.css" type="text/css" rel="stylesheet">
	</head>

	<body>
		<div id = "banner">
			<img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/banner.png" alt="Rancid Tomatoes">
		</div>
            <?php 
               $string_array = file($film . "\info.txt", FILE_IGNORE_NEW_LINES);
               list($film_title, $film_date, $film_score) = $string_array;
            ?>
            <h1> <?= $film_title ?> (<?= $film_date ?>)</h1>
       
        <div class = "content">
            
            <div class = "section">    
            <div>
                <?php
                    $pathImage = $film ."\overview.png";
                ?>
                <img src = "<?= $pathImage?>" alt = "film cover" id = "film_image">
            </div>

            <dl>
                
                <?php
                
                $string_overview = file_get_contents($film . "\overview.txt");
                $array_string_overview = explode("\n", $string_overview);
                for ($i = 0; $i < count($array_string_overview); $i++) {
                    $list_string = explode(':', $array_string_overview[$i]);
                ?>
                    <dt><?= $list_string[0] ?></dt>
                    <dd><?= $list_string[1]?></dd>
              <?php
                }
               ?> 
            </dl>
            </div>
        
            
            <div id = "border">
                <?php
                    $img_path = 'http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/rottenbig.png';
                    if($film_score >= 60){
                        $img_path = 'https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/freshbig.png';
                    }    
                
                ?>
                <img id  = "border-img" src="<?= $img_path ?>" alt="Rotten">
                <div id = "number"><?= $film_score?>%</div>
            </div>
            
                      
            
        <div id = "rotten">  
            <div class = "first-column"> 
                <?php
                
                    $all_reviews_path = glob($film . '\review*.txt');
                    $one_review = NULL;
                  
                    for($i = 0; $i < ceil(count($all_reviews_path)/2); $i++){
                        $one_review = file($all_reviews_path[$i], FILE_IGNORE_NEW_LINES);
                        list($comment, $mark, $name, $site) = $one_review;
                        
                         $img_vote_path = 'http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/rotten.gif';
                         if(!strcmp($mark, 'FRESH')){
                            $img_vote_path = 'https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/fresh.gif';     
                   }
                 ?>
            
           
                <div class = "review">
                    <img src= "<?= $img_vote_path?>" alt="img vote">
                    <q> <?= $comment ?> </q>
                </div>
                <div class = "author">
                    <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif" alt="Critic">
                    <?= $name ?> <br>
                    <span class = "publisher"><?= $site?> </span>
                </div>
            <?php
                }
            ?>
            </div>
            
            <div class = "second-column">
                <?php
                    for(; $i < count($all_reviews_path); $i++){
                       $one_review = file($all_reviews_path[$i], FILE_IGNORE_NEW_LINES);
                       list($comment, $mark, $name, $site) = $one_review;
                       $img_vote_path = 'http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/rotten.gif';
                         if(!strcmp($mark, 'FRESH')){
                            $img_vote_path = 'https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/fresh.gif';
                         }    
                ?>
                <div class = "review">
                    <img src="<?= $img_vote_path?>" alt="img vote">
                    <q> <?= $comment ?> </q>
                </div>
                <div class = "author">
                    <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif" alt="Critic">
                    <?= $name ?> <br>
                    <span class = "publisher"><?= $site?> </span>
                </div>
            <?php
                }
            ?>
                
            </div>
    </div>  
             <div id = "num-pag">(1-<?=count($all_reviews_path)?>) of <?= count($all_reviews_path) ?></div>
    </div>

		<div id = "validator">
			<a href="ttp://validator.w3.org/check/referer"><img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/w3c-xhtml.png" alt="Validate HTML">></a> <br>
			<a href="http://jigsaw.w3.org/css-validator/check/referer"><img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!"></a>
		</div>
	</body>
</html>
