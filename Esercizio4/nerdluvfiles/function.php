<?php

function find_user($all_user_data){
    $user_name = $_GET['user_name'];
    $this_user_data = NULL;
    $i = 0;
    while($i < count($all_user_data)){
        $this_user_data = explode(',', $all_user_data[$i]);
        if(!strcmp($user_name, $this_user_data[0])){
            break;
        }
        else{
            $this_user_data = NULL;
        }
        $i++;
    }
    return $this_user_data;
}



function find_all_matches($this_user_data, $all_user_data){
 
    $user_matched = [];
    for($i = 0; $i < count($all_user_data); $i++){
        $one_user_data = explode(',', $all_user_data[$i]);
        if(check_if_matches($one_user_data, $this_user_data)){
            $user_matched[] = $one_user_data;   
        }
    }
    return $user_matched;
}

function check_if_matches($one_user_data, $this_user_data){
    if($this_user_data[1] == 'M'){
        $gender = 'F';
    }
    else{
        $gender = 'M';
    }
    
    if($one_user_data[1] != $gender){
        
        return false;
    }
   
    if(!($one_user_data[2] >= $this_user_data[5] && $one_user_data[2] <=
    $this_user_data[6])){
        return false;
    }
    if(strcmp($one_user_data[4], $this_user_data[4])){
        return false;
    }
    for($i = 0; $i < 4; $i++){
        if($one_user_data[3][$i] == $this_user_data[3][$i]){
            return true;
        }
    }
    return false;   
}



