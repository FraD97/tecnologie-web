
<!DOCTYPE html>
<html lang="en">
    <head>
		<title>NerdLuv</title>
		<meta charset="utf-8" >
		
		<!-- instructor-provided CSS and JavaScript links; do not modify -->
		<link href="http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/4/heart.gif" type="image/gif" rel="shortcut icon" >
		<link href="nerdluv.css" type="text/css" rel="stylesheet" >
        
    </head>
    
  
    
    <body>
        <?php   include_once 'top.html';?>
        <form action ="signup-submit.php" method="post">
            <fieldset>
                <legend>New User Signup</legend>
                Name: <input type = "text" name = "name"> <br>
                Gender: <input type ="radio" name ="gender" value="M"> Male 
                        <input type ="radio" name ="gender" value="F"> Female <br>
                        Age: <input type="text" name = "user_age" size="6" maxlength="2"><br> 
                Personality type: <input type="text" name = "personality" size ="10" maxlength="4">
                                         (Don't know your type?) <br>
              Favorite OS: <select name = "favourite_os">
                                <option value ="Mac OS X">Mac OS X</option>
                                <option value ="Windows">Windows</option>
                                <option value ="Linux">Linux</option>
                          </select> <br>
                          Seeking age: <input type="text" name ="min_age" placeholder="18" size="6" maxlength="2"> to
                          <input type ="text" name ="max_age" placeholder="99" size="6" maxlength="2"> <br>
              <input type="submit" value="Sign Up">
        </fieldset>
            
        </form> 
        <?php   include_once 'bottom.html';?>
    </body>
       
</html>


