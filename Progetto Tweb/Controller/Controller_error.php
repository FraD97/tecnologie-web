<?php

class Controller_error {

    public function __construct() {

    }
    
    public function invoke(){
        session_start();
        if(!isset($_SESSION['id'])){
            $this->redirect("login.php", "Effettua il login per accedere al sito");
        }
        else{
            include '../View/header.html';
            if(isset($_GET['code']) && $_GET['code'] == 500){   
                include '../View/error/500.html';
            }
            else if(isset($_GET['code']) && $_GET['code'] == 400){
                include '../View/error/400.html';
            }
            else if(isset($_GET['code']) && $_GET['code'] == 404){
                include '../View/error/announcement_not_found.html';
            }
            else{
                include '../View/error/404.html';
            }
            include '../View/footer.html';
        }
    }
     
    private function redirect($url, $flash_message = NULL) {
        if ($flash_message) {
            $_SESSION["flash"] = $flash_message;
        }
        header("Location: $url");
        die;
    }
    
}
