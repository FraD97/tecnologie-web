<?php

include '../Model/Model.php';

class Controller_my_announcements {

    private $model;

    function __construct() {
        $this->model = new Model();
    }

    public function invoke() {
        session_start();
        if (isset($_SESSION['id'])) {
            $profile = $this->model->get_user_profile($_SESSION['id']);
            if ($profile === null) {
                header("Location: ../src/error.php?code=400");
                die;
            }
            $active_announcements = $this->model->get_number_announcement_by_id($_SESSION['id']);
            if ($active_announcements === null) {
                header("Location: ../src/error.php?code=400");
                die;
            }
            $ended_announcements = $this->model->get_number_ended_announcements_by_id($_SESSION['id']);
            if ($ended_announcements === null) {
                header("Location: ../src/error.php?code=400");
                die;
            }
            include '../View/header.html';
            include '../View/my_announcement_info.php';
            include '../View/footer.html';
        } else {
            $this->redirect("login.php", "Effettua il login per accedere al sito");
        }
    }

    private function redirect($url, $flash_message = NULL) {
        if ($flash_message) {
            $_SESSION["flash"] = $flash_message;
        }
        header("Location: $url");
        die;
    }

}
