<?php
/*
 * Il seguente controller si occupa di validare la creazione di un annuncio
 */
include '../Model/Model.php';

class Controller_announcement {

    private $model;

    public function __construct() {
        $this->model = new Model();
    }

    public function invoke() {
        $id = $this->validate_fields();
        $_SESSION['announce'] = $id;
        header("Location: ../src/index.php");
        exit();
    }

    public function validate_fields() {
        if (isset($_POST['title']) && isset($_POST['description']) && isset($_POST['price']) && isset($_POST['number']) && isset($_POST['mail']) && isset($_POST['category']) && isset($_POST['region']) && file_exists($_FILES['file']['tmp_name'][0])) {
            session_start();
            $title = filter_var($_POST['title'], FILTER_SANITIZE_STRING);
            $description = filter_var($_POST['description'], FILTER_SANITIZE_STRING);
            $price = filter_var($_POST['price'], FILTER_SANITIZE_NUMBER_FLOAT);
            $number = filter_var($_POST['number'], FILTER_VALIDATE_INT);
            $mail = filter_var($_POST['mail'], FILTER_SANITIZE_EMAIL);
            $category = filter_var($_POST['category'], FILTER_SANITIZE_STRING);
            $region = filter_var($_POST['region'], FILTER_SANITIZE_STRING);
            $this->check_input($title, $description, $price, $number, $mail, $category, $region);
            $count = $this->check_uploaded_file();
            $announce_id = $this->model->insert_announce($title, $_SESSION['id'], $description, $price, $number, $mail, $category, $region);
            if ($announce_id == -1) {
                header("Location: ../src/error.php?code=400");
                die;
            }
            $this->move_file($count, $announce_id);
            return $announce_id;
        } else {
            header("Location: ../src/error.php?code=400");
            die;
        }
    }

    public function check_input($title, $description, $price, $number, $mail, $category, $region) {
        if (strlen($title) < 2 || strlen($title) > 30 || strlen($description) > 250 || $number == false || strlen($description) < 50 || $price > 999999 || $this->check_email($mail) || $this->check_category_and_region($category, $region)) {
            header("Location: ../src/error.php?code=400");
            die;
        }
    }

    public function check_email($email) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
    }

    private function check_category_and_region($category, $region) {
        $result1 = $this->model->find_category($category);
        $result2 = $this->model->find_region($region);
        if($result1 == -1 || $result2 == -1){
            header("Location: ../src/error.php?code=500");
            die;
        }
        else if($result1 + $result2 != 2){
            header("Location: ../src/error.php?code=400");
            die;
        }
        else{
            return false;
        }
    }

    public function check_select($category, $region) {
        return false;
    }

    public function check_uploaded_file() {
        if (isset($_FILES['file'])) {
            if (!file_exists($_FILES['file']['tmp_name'][0])) {
                header("Location: ../src/error.php?code=400");
                die;
            } else {
                $count = count($_FILES['file']['tmp_name']);
                if ($count > 3) {
                    header("Location: ../src/error.php?code=400");
                    die;
                }
                for ($i = 0; $i < $count; $i++) {
                    if (!file_exists($_FILES['file']['tmp_name'][$i]) || !getimagesize($_FILES["file"]["tmp_name"][$i])) {
                        header("Location: ../src/error.php?code=400");
                        die;
                    }
                }
                return $count;
            }
        }
    }

    public function move_file($count, $announce_id) {
        $uploaddir = '../img/announce';
        for ($i = 0; $i < $count; $i++) {
            $userfile_tmp = $_FILES['file']['tmp_name'][$i];
            $extension = $this->findexts($_FILES['file']['name'][$i]);
            $my_id = $_SESSION['id'];
            move_uploaded_file($userfile_tmp, $uploaddir . "/" . $my_id . 'A' . $announce_id . '_' . $i . '.' . $extension);
        }
    }

    private function findexts($filename) {
        $filename = strtolower($filename);
        $exts = explode(".", $filename);
        $n = count($exts) - 1;
        $exts = $exts[$n];
        return $exts;
    }
}

$controller = new Controller_announcement();
$controller->invoke();
