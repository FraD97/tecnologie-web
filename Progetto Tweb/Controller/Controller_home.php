<?php

include '../Model/Model.php';

class Controller_home {
    private $model;
    
    public function __construct() {
        $this->model = new Model();
    }
    
    public function invoke(){
        session_start();
        if(!isset($_SESSION['id'])){
            $this->redirect("login.php", "Effettua il login per accedere al sito");
        }
        else{
            $announcements = $this->model->get_recent_announcements();
            if($announcements === null){
                header("Location: ../src/error.php?code=500");
                die;
            }
            $n = count($announcements);
            include'../View/header.html';
            include'../View/searchbar.html';
            include'../View/index_middle.php';
            include'../View/footer.html';
            if(isset($_SESSION['announce'])){
                include '../View/snackbar.php';
                unset($_SESSION['announce']);
            }
        }
    }
     
    private function redirect($url, $flash_message = NULL) {
        if ($flash_message) {
            $_SESSION["flash"] = $flash_message;
        }
        header("Location: $url");
        die;
    }
    
}
