<?php

class Controller_rl {

    public function __construct() {
        
    }

    public function invoke_l() {
        session_start();
        if (isset($_SESSION['id'])) {
            $this->redirect("index.php", "Effettua prima il logout");
        } else {
            include '../View/login_form.php';
        }
    }

    public function invoke_r() {
        session_start();
        if (isset($_SESSION['id'])) {
            $this->redirect("index.php", "Effettua prima il logout");
        } else {
            include '../View/register_form.html';
        }
    }

    private function redirect($url, $flash_message = NULL) {
        if ($flash_message) {
            $_SESSION["flash"] = $flash_message;
        }
        header("Location: $url");
        die;
    }

}
