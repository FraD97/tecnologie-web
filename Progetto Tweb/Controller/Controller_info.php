<?php

include '../Model/Model.php';

class Controller_info {

    private $model;

    public function __construct() {
        $this->model = new Model();
    }

    public function invoke() {
        session_start();
        if (!isset($_SESSION['id'])) {
            $this->redirect("login.php", "Effettua il login per accedere al sito");
        } else {
            if (isset($_GET['id'])) {
                $announcement = $this->check_id();
                include '../View/header.html';
                include '../View/announce_data.php';
                include '../View/footer.html';
            } else {
                header("Location: ../src/error.php?code=500");
                die;
            }
        }
    }

    private function get_username($id) {
        return $this->model->get_username($id);
    }

    private function check_id() {
        $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
        if ($id) {
            $announcement = $this->model->get_announcement($id);
            if ($announcement) {
                $this->check_announcement($announcement);
                return $announcement;
            } else {
                header("Location: ../src/error.php?code=404");
                die;
            }
        } else {
            header("Location: ../src/error.php?code=400");
            die;
        }
    }

    private function redirect($url, $flash_message = NULL) {
        if ($flash_message) {
            $_SESSION["flash"] = $flash_message;
        }
        header("Location: $url");
        die;
    }

    private function check_announcement($announcement) {
        if ($announcement->get_state() == 'ended') {
            if($announcement->get_user_id() === $_SESSION['id']){
                return;
            }
            header("Location: ../src/error.php?code=404");
            die;
        }
    }
}
