$(document).ready(function () {
    register();
});

function register() {
    var submitButton = $("#register_button");
    var nameInput = $("#name_field");
    var surnameInput = $("#surname_field");
    var usernameInput = $("#username_field");
    var emailInput = $("#email_field");
    var passwordInput = $("#password_field");
    var passwordConfirmInput = $("#passwordconfirm_field");
    var numberInput = $("#number_field");
    setHints(nameInput, surnameInput, usernameInput, emailInput, passwordInput);
    submitButton.on("click", function () {
        validateAll(nameInput, surnameInput, usernameInput, emailInput, passwordInput, passwordConfirmInput, numberInput);
    });
}

function validateAll(name, surname, username, email, passwordInput, passwordConfirmInput, number) {
    var resultName = validateName(name, "Nome");
    var resultSurname = validateName(surname, "Cognome");
    var resultPasswords = checkIfPasswordsMatches(passwordInput, passwordConfirmInput);
    var resultUsername = validateUsername(username);
    var resultMail = validateEmail(email);
    var resultNumber = validateNumber(number);
    if (resultUsername && resultMail && resultName && resultSurname && resultPasswords && resultNumber) {
        checkForExistingUsers(username, email, name, surname, passwordInput, number.val() || -1);
    }
}
function validateNumber(number) {
    if (number.val().length > 0 && Number.isInteger(parseInt(number.val()))) {
        giveUserFeedback(number, "Numero valido", 1);
        return true;
    } else if (number.val().length === 0) {
        return true;
    } else {
        giveUserFeedback(number, "Numero non valido");
        return -1;
    }

}

function setHints(name, surname, username, mail, password) {
    (name).add(surname).add(username).add(mail).add(password).popover({trigger: 'focus'});
    (name).add(surname).add(username).add(mail).add(password).blur(function () {
        $(this).popover("hide");
    });
}

function validateUsername(username) {
    var re = /^[a-z]{2,}\d*$|(?=\w{3,})^[a-z]{1,}\d+$/i;
    if (username.val()) {
        if (re.test(username.val())) {
            giveUserFeedback(username, "Username valido", 1);
            return 1;
        } else {
            giveUserFeedback(username, "Username non valido");
        }
    } else {
        giveUserFeedback(username, "Per favore inserisci un Username");
    }
}

function checkIfPasswordsMatches(p1, p2) {
    var p1Result = validatePassword(p1);

    if (p1.val() !== p2.val()) {
        giveUserFeedback(p2, "Le due password non corrispondono");
    } else if (p1Result) {
        giveUserFeedback(p2, "Per favore rispetta il formato richiesto");
    } else {
        giveUserFeedback(p2, "Le password corrispondono", 1);
        return 1;
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email.val()) {
        if (re.test(email.val())) {
            giveUserFeedback(email, "Mail valida", 1);
            return 1;
        } else {
            giveUserFeedback(email, "Mail non valida");
            return 0;
        }
    } else {
        giveUserFeedback(email, "Per favore inserisci la tua Email");
    }
}

function validateName(nameInput, name) {
    if (nameInput.val()) {
        if (nameInput.val().length > 1) {
            giveUserFeedback(nameInput, name + " valido", 1);
            return 1;
        } else {
            giveUserFeedback(nameInput, name + " non valido");
        }
    } else {
        giveUserFeedback(nameInput, "Per favore inserisci il tuo " + name);
    }
}

function validatePassword(password) {
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    if (password.val()) {
        if (re.test(password.val())) {
            giveUserFeedback(password, "Password inserita correttamente" + name, 1);
        } else {
            giveUserFeedback(password, "La password non rispetta il formato richiesto" + name);
            return 1;
        }
    } else {
        giveUserFeedback(password, "Per favore inserisci una password" + name);
        return 1;
    }
}

function giveUserFeedback(input, text, state) {
    var feedbackText = $("#" + input.attr("id") + " + div");
    feedbackText.html(text);
    if (state === 1) {
        input.attr("class", "form-control is-valid");
        feedbackText.attr("class", "valid-feedback");
    } else {
        input.attr("class", "form-control is-invalid");
        feedbackText.attr("class", "invalid-feedback");
    }
}

function checkForExistingUsers(username, email, name, surname, password, number) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: '../Json/json_costumers.php',
        data: {
            username: username.val(),
            email: email.val(),
            name: name.val(),
            surname: surname.val(),
            password: password.val(),
            number: number
        },
        success: function (result) {
            if (result) {
                if (result === 1) {
                    giveUserFeedback(email, "Esiste già un utente con questa mail");
                } else if (result === 2) {
                    giveUserFeedback(username, "Esiste già un utente con questo nome");
                } else if (result === 3) {
                    giveUserFeedback(email, "Esiste già un utente con questa mail");
                    giveUserFeedback(username, "Esiste già un utente con questo nome");

                } else if (result === -1) {
                    alert("Si è verificato un errore nella registrazione dell'utente, riprovare più tardi");
                } else {
                    giveUserFeedback(email, "Esiste già un utente con questa mail");
                }
            } else {
                window.setTimeout(function () {
                    window.location = "login.php";
                }, 500);
            }
        },
        error: function () {
            alert("Purtroppo la richiesta non può essere soddisfatta. Riprova più tardi");
        }
    });
}
