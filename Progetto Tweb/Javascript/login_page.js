$(document).ready(function () {
    register();

});

function register() {

    button_login = $("#login_button");
    button_login.click(function () {
        var feedbackUsername = $("#username_field + div");
        var feedbackPassword = $("#password_field + div");
        var username = $('#username_field');
        var password = $('#password_field');
        if (username.val() && password.val()) {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: '../Json/json_costumers.php',
                data: {
                    username: username.val(),
                    password: password.val()
                },
                success: function (result) {
                    if (result) {
                        window.location.href = "../src/index.php";

                    } 
                    else if(result === -1){
                        username.attr("class", "form-control is-invalid");
                        password.attr("class", "form-control is-invalid");
                        feedbackUsername.html("Si è verificato un errore di connessione").attr("class", "invalid-feedback");
                        feedbackPassword.html("Si è verificato un errore di connessione").attr("class", "invalid-feedback");
                    }
                    else {
                        username.attr("class", "form-control is-invalid");
                        password.attr("class", "form-control is-invalid");
                        feedbackUsername.html("Username e/o password errati").attr("class", "invalid-feedback");
                        feedbackPassword.html("Username e/o password errati").attr("class", "invalid-feedback");
                    }
                },
                error: function () {
                    alert("Purtroppo la richiesta non può essere soddisfatta. Riprova più tardi");
                }
            });
        } else {
            if (!username.val()) {
                feedbackUsername.html("Inserisci il tuo Username").attr("class", "invalid-feedback");
                username.attr("class", "form-control is-invalid");
            }
            if (!password.val()) {
                feedbackPassword.html("Inserisci la tua password").attr("class", "invalid-feedback");
                password.attr("class", "form-control is-invalid");
            }
        }
    });
}

