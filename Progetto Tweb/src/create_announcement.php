<!DOCTYPE html>
<?php
include '../Controller/Controller_create_announcement.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Crea Annuncio</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../style/header.css">
        <link rel="stylesheet" type="text/css" href="../style/common.css">
        <link rel="stylesheet" type="text/css" href="../style/footer.css">
        <link rel="stylesheet" type="text/css" href="../style/create_announcement.css">
        <link rel="stylesheet" type="text/css" href="../style/info_announcement.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="../Javascript/create_announcement.js" type="text/javascript"></script>
        <script src="../Javascript/menu.js" type="text/javascript"></script>
    </head>
    <body>
        <?php
        $controller = new Controller_create_announcement();
        $controller->invoke();
        ?>
    </body>
</html>
