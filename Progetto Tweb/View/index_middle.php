<div id = "container_column">       
    <div id="column2">
        <div>
            <div class="hint_top">Le categorie più diffuse</div>
            <div id="categories">
                <div id="c1" class="category_button">
                    <img src="../img/elettronica.ico" alt="elettronica_icon">
                    <span class="title_category">Elettronica</span>
                </div>
                <div id="c2" class="category_button">
                    <img  src="../img/car.ico" alt="car_icon">
                    <span class="title_category">Automobili</span>
                </div>
                <div id="c3" class="category_button">
                    <img  src="../img/game.ico" alt="game_icon">
                    <span class="title_category">Videogiochi</span>
                </div>
                <div id="c4" class="category_button">
                    <img  src="../img/smartphone.ico" alt="smartphone_icon">
                    <span class="title_category">Telefonia</span>
                </div>
                <div id="c5" class="category_button">
                    <img  src="../img/guitar.ico" alt="guitar_icon">
                    <span class="title_category">Musica</span>
                </div>
                <div id="c6" class="category_button">
                    <img src="../img/ball.ico" alt="ball_icon">
                    <span class="title_category">Sport</span>
                </div>
                <div id="c7" class="category_button">
                    <img  src="../img/mac.ico" alt="mac_icon">
                    <span class="title_category">Computer</span>
                </div>
                <div id="c8" class="category_button">
                    <img src="../img/furniture.ico" alt="furniture_icon" >
                    <span class="title_category">Per la casa</span>
                </div>
                <div  id="c9" class="category_button">
                    <img src="../img/all.ico" alt="all_icon">
                    <span class="title_category">Vedi tutto</span>
                </div>
            </div>
        </div>
        <div>
            <div class="hint_top">Annunci recenti</div>
            <div id="recent">
                <?php include '../View/carousel_index.php'; ?>
            </div>
        </div>
    </div> 
    <div id="column1">
        <div class="hint_top">Seleziona regione</div>
        <div id="regions_container">
            <table>
                <tr>
                    <td> <a href="search.php?search_text=&region=Piemonte&category=all&page=0"><div>Piemonte</div></a></td>
                    <td> <a href="search.php?search_text=&region=Val d'Aosta&category=all&page=0"><div>Val d'Aosta</div></a></td>
                </tr>
                <tr>
                    <td> <a href="search.php?search_text=&region=Lombardia&category=all&page=0"><div>Lombardia</div></a></td>
                    <td> <a href="search.php?search_text=&region=Trentino Alto-Adige&category=all&page=0"><div>Trentino Alto-Adige</div></a></td>
                </tr>
                <tr>
                    <td> <a href="search.php?search_text=&region=Friuli-Venezia Giulia&category=all&page=0"><div>Friuli-Venezia Giulia</div></a></td>
                    <td> <a href="search.php?search_text=&region=Liguria&category=all&page=0"><div>Liguria</div></a></td>
                </tr>
                <tr>
                    <td> <a href="search.php?search_text=&region=Veneto&category=all&page=0"><div>Veneto</div></a></td>
                    <td> <a href="search.php?search_text=&region=Emilia-Romagna&category=all&page=0"><div>Emilia-Romagna</div></a></td>
                </tr>
                <tr>
                    <td> <a href="search.php?search_text=&region=Toscana&category=all&page=0"><div>Toscana</div></a></td>
                    <td> <a href="search.php?search_text=&Marche=Piemonte&category=all&page=0"><div>Marche</div></a></td>
                </tr>
                <tr>
                    <td> <a href="search.php?search_text=&region=Umbria&category=all&page=0"><div>Umbria</div></a></td>
                    <td> <a href="search.php?search_text=&region=Lazio&category=all&page=0"><div>Lazio</div></a></td>
                </tr>
                <tr>
                    <td> <a href="search.php?search_text=&region=Abruzzo&category=all&page=0"><div>Abruzzo</div></a></td>
                    <td> <a href="search.php?search_text=&region=Molise&category=all&page=0"><div>Molise</div></a></td>
                </tr>
                <tr>
                    <td> <a href="search.php?search_text=&region=Campania&category=all&page=0"><div>Campania</div></a></td>
                    <td> <a href="search.php?search_text=&region=Puglia&category=all&page=0"><div>Puglia</div></a></td>
                </tr>
                <tr>
                    <td> <a href="search.php?search_text=&region=Basilicata&category=all&page=0"><div>Basilicata</div></a></td>
                    <td> <a href="search.php?search_text=&region=Calabria&category=all&page=0"><div>Calabria</div></a></td>
                </tr>
                <tr>
                    <td> <a href="search.php?search_text=&region=Sicilia&category=all&page=0"><div>Sicilia</div></a></td>
                    <td> <a href="search.php?search_text=&region=Sardegna&category=all&page=0"><div>Sardegna</div></a></td>
                </tr>
            </table>
            <a href="search.php?search_text=&region=all&category=all&page=0">Tutta Italia</a>
        </div>
    </div>
</div>
