<?php

class db_connector {

    private $db_name;
    private $db_type;
    private $db_host;
    private $db_username;
    private $db_password;

    public function __construct() {
        $db_path = '../Database/db_attributes.csv';
        $db_attributes_data = file_get_contents($db_path);
        $string_array_db = explode(',', $db_attributes_data);
        list($this->db_type, $this->db_name, $this->db_host, $this->db_username, $this->db_password) = $string_array_db;
    }

    public function connect() {
        try{
        $db = new PDO($this->db_type . ':dbname=' . $this->db_name . ';host:' . $this->db_host, $this->db_username, $this->db_password);
        return $db;
        }
        catch(PDOException $ex){
            header("Location: ../src/error.php?code=500");
        }
    }

}
