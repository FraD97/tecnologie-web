<?php
include '../Model/Model.php';

session_start();
if (!isset($_SESSION['id'])) {
    header('Content-Type: application/json');
    echo json_encode(false);
    die;
}

if (isset($_GET['setting']) && isset($_GET['offset']) && $_GET['setting'] == 1) {
    $model = new Model();
    $id = $_SESSION['id'];
    $offset = $_GET['offset'];
    if (filter_var($offset, FILTER_VALIDATE_INT) === 0 || filter_var($offset, FILTER_VALIDATE_INT)) {
        $announcements = $model->get_announcement_by_id($id, $offset);
        if ($announcements === null) {
            header('Content-Type: application/json');
            echo json_encode(false);
        }
        header('Content-Type: application/json');
        echo json_encode($announcements);
    } else {
        header('Content-Type: application/json');
        echo json_encode(false);
    }
} else if (isset($_GET['search_text']) && isset($_GET['region']) && isset($_GET['category']) && isset($_GET['page'])) {
    $text = filter_var($_GET['search_text'], FILTER_SANITIZE_STRING);
    $region = filter_var($_GET['region'], FILTER_SANITIZE_STRING);
    $category = filter_var($_GET['category'], FILTER_SANITIZE_STRING);
    $page = $_GET['page'];
    $announcements_data = validate_searchbar_fields($text, $category, $region, $page);
    header('Content-Type: application/json');
    echo json_encode($announcements_data);
} else if (isset($_POST['announcement_id'])) {
    $announcement_id = filter_var($_POST['announcement_id'], FILTER_VALIDATE_INT);
    if ($announcement_id) {

        $model = new Model();
        if ($model->remove_announcement_try($announcement_id)) {
            $model->update_announcement($announcement_id);
            header('Content-Type: application/json');
            echo json_encode(true);
        } else {
            header('Content-Type: application/json');
            echo json_encode(false);
        }
    } else {
        header('Content-Type: application/json');
        echo json_encode(false);
    }
} else if (isset($_GET['setting']) && isset($_GET['offset']) && $_GET['setting'] == 2) {

    $model = new Model();
    $offset = $_GET['offset'];
    $id = $_SESSION['id'];
    if (filter_var($offset, FILTER_VALIDATE_INT) === 0 || filter_var($offset, FILTER_VALIDATE_INT)) {
        $announcements = $model->get_announcement_ended_by_id($id, $offset);
        header('Content-Type: application/json');
        echo json_encode($announcements);
    }
} else if (isset($_GET['id'])) {
    $id = $_GET['id'];
    if (filter_var($id, FILTER_VALIDATE_INT) === 0 || filter_var($id, FILTER_VALIDATE_INT)) {
        $model = new Model();
        $announcement = $model->get_announcement($id);
        if ($announcement === null) {
            header('Content-Type: application/json');
            echo json_encode(false);
            die;
        }
        return $announcement;
    } else {
        header('Content-Type: application/json');
        echo json_encode($announcements);
    }
} else {
    header('Content-Type: application/json');
    echo json_encode(false);
}

function validate_searchbar_fields($text, $category, $region, $page) {
    $model = new Model();
    if (filter_var($page, FILTER_VALIDATE_INT) === 0 || filter_var($page, FILTER_VALIDATE_INT)) {
        $announcements_data = $model->get_announcement_searchbar($text, $category, $region, $page);
        if ($announcements_data === null) {
            header('Content-Type: application/json');
            echo json_encode(false);
            die;
        }
        return $announcements_data;
    } else {
        header('Content-Type: application/json');
        echo json_encode(false);
        die;
    }
}
