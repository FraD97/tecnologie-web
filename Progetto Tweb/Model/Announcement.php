<?php

/*
 * La seguente classe rappresenta un Annuncio del sito.
 */

class Announcement implements JsonSerializable {

    private $id;
    private $user_id;
    private $title;
    private $description;
    private $price;
    private $number;
    private $mail;
    private $category;
    private $region;
    private $time_announce;
    private $state;

    public function jsonSerialize() {
        return get_object_vars($this);
    }

    public function get_user_id(){
        return $this->user_id;
    }

    public function get_id() {
        return $this->id;
    }

    public function get_title() {
        return $this->title;
    }
    
    public function get_description(){
        return $this->description;
    }
    
    public function get_price(){
        return $this->price;
    }
    
    public function get_number(){
        return $this->number;
    }
    
    public function get_mail(){
        return $this->mail;
    }
    
    public function get_category(){
        return $this->category;
    }
    
    public function get_region(){
        return $this->region;
    }
    public function get_state(){
        return $this->state;
    }

}
