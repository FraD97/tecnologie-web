<?php

include '../Database/db_connector.php';
include 'Costumer.php';
include 'Announcement.php';

class Model {

    private $db;

    function __construct() {
        $db_connector = new db_connector();
        $this->db = $db_connector->connect();
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function get_all_costumers() {
        try {
            $query = 'SELECT id, id, name, surname, username, email FROM costumer';
            $costumers = $this->db->query($query)->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_UNIQUE, 'Costumer');
            return $costumers;
        } catch (PDOException $ex) {
            return null;
        }
    }

    public function get_costumer_data($id) {
        try {
            $id = $this->db->quote($id);
            $query = 'SELECT email, number FROM costumer WHERE id = ' . $id;
            $costumer = $this->db->query($query)->fetchObject('Costumer');
            return $costumer;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function get_costumer_id($username, $password) {
        try {
            $username = $this->db->quote($username);
            $password = $this->db->quote($password);
            $query = 'SELECT id FROM costumer WHERE username =' . $username .
                    ' AND password = ' . $password;
            return $id = $this->db->query($query)->fetchColumn();
        } catch (PDOException $ex) {
            return -1;
        }
    }

    public function check_for_existing_mail($email) {
        try {
            $email = $this->db->quote($email);
            $query = 'SELECT email FROM costumer WHERE email LIKE ' . $email;
            $result = $this->db->query($query);
            if ($result->rowCount()) {
                return true;
            }
            return false;
        } catch (PDOException $ex) {
            return -1;
        }
    }

    public function check_for_existing_user($user) {
        try {
            $user = $this->db->quote($user);
            $query = 'SELECT email FROM costumer WHERE username LIKE ' . $user;
            $result = $this->db->query($query);
            if ($result->rowCount()) {
                return true;
            }
            return false;
        } catch (PDOException $ex) {
            return -1;
        }
    }

    public function get_username($id) {
        try {
            $id = $this->db->quote($id);
            $query = "SELECT username FROM costumer WHERE id LIKE " . $id;
            return $this->db->query($query)->fetchColumn();
        } catch (PDOException $ex) {
            return -1;
        }
    }

    public function get_user_profile($id) {
        try {
            $id = $this->db->quote($id);
            $query = "SELECT name, surname, username, email FROM costumer WHERE id LIKE " . $id;
            return $this->db->query($query)->fetchObject("Costumer");
        } catch (PDOException $ex) {
            return null;
        }
    }

    public function insert_costumer($name, $surname, $email, $username, $password) {
        try {
            $name = $this->db->quote($name);
            $surname = $this->db->quote($surname);
            $username = $this->db->quote($username);
            $email = $this->db->quote($email);
            $password = $this->db->quote($password);
            $query = 'INSERT INTO costumer (name, surname, username, email, password) values (' . $name . ', ' . $surname . ', ' . $username . ', ' . $email . ',' . $password . ')';
            $this->db->query($query);
        } catch (PDOException $ex) {
            return -1;
        }
    }

    public function insert_costumer_number($name, $surname, $email, $username, $password, $number) {
        try {
            $name = $this->db->quote($name);
            $surname = $this->db->quote($surname);
            $username = $this->db->quote($username);
            $email = $this->db->quote($email);
            $password = $this->db->quote($password);
            $number = $this->db->quote($number);
            $query = 'INSERT INTO costumer (name, surname, username, email, number, password) values (' . $name . ', ' . $surname . ', ' . $username . ', ' . $email . ', ' . $number . ',' . $password . ')';
            $this->db->query($query);
            return 0;
        } catch (PDOException $ex) {
            return -1;
        }
    }

    public function insert_announce($title, $user_id, $description, $price, $number, $mail, $category, $region) {
        try {
            $title = $this->db->quote($title);
            $description = $this->db->quote($description);
            $price = $this->db->quote($price);
            $number = $this->db->quote($number);
            $mail = $this->db->quote($mail);
            $category = $this->db->quote($category);
            $region = $this->db->quote($region);
            $query = 'insert INTO announcement(user_id, title, description, price, number, mail, category, region, time_announce) values(' . $user_id . ',' . $title . ',' . $description . ',' . $price . ',' . $number . ',' . $mail . ',' . $category . ',' . $region . ',NOW())';
            $this->db->query($query);
            return $this->db->lastInsertId();
        } catch (PDOException $ex) {
            return -1;
        }
    }

    public function get_number_announcement() {
        try {
            $query = 'SELECT count(id) FROM announcement';
            $count = $this->db->query($query)->fetchColumn();
            return $count;
        } catch (PDOException $ex) {
            return -1;
        }
    }
    
    
/*
 * La seguente funzione ritorna gli annunci compresi da offset a offset +8, ed il numero di annunci totali attivi presenti corrispondenti
 * alle caratteristiche selezionate
 */
    public function get_announcement_searchbar($text, $category, $region, $offset) {
        try {
            $i = 0;
            $offset *= 8;
            $sub_query = '1';
            if ($text != '') {
                $text = '%' . $text . '%';
                $text = $this->db->quote($text);
                $sub_query = 'title LIKE' . $text;
                $i++;
            }
            if ($category != 'all') {
                $category = $this->db->quote($category);
                if ($i == 1) {
                    $sub_query .= ' AND category LIKE ' . $category;
                    $i++;
                } else {
                    $sub_query = 'category LIKE ' . $category;
                    $i++;
                }
            }
            if ($region != 'all') {
                $region = $this->db->quote($region);
                if ($i >= 1) {
                    $sub_query .= ' AND region LIKE ' . $region;
                } else {
                    $sub_query = 'region LIKE ' . $region;
                }
            }
            $query_1 = "SELECT * FROM announcement WHERE " . $sub_query . ' AND state = "active" ORDER BY id DESC LIMIT 8 OFFSET ' . $offset;
            $query_2 = 'SELECT count(id) FROM announcement WHERE state = "active" AND ' . $sub_query;
            $announcements_data = array();
            $announcements_data[0] = $this->db->query($query_1)->fetchAll(PDO::FETCH_CLASS, "Announcement");
            $announcements_data[1] = $this->db->query($query_2)->fetchColumn();
            return $announcements_data;
        } catch (PDOException $ex) {
            return null;
        }
    }

    public function get_announcement($id) {
        try {
            $id = $this->db->quote($id);
            $query = 'SELECT * FROM announcement WHERE id =' . $id;
            return $announce = $this->db->query($query)->fetchObject("Announcement");
        } catch (PDOException $ex) {
            return null;
        }
    }

    public function get_announcement_by_id($id, $offset) {
        try {
            $id = $this->db->quote($id);
            $query = 'SELECT * FROM announcement WHERE state = "active" AND user_id=' . $id . ' ORDER BY id DESC LIMIT 6 OFFSET ' . $offset;
            $announcements = $this->db->query($query)->fetchAll(PDO::FETCH_CLASS, "Announcement");
            return $announcements;
        } catch (PDOException $ex) {
            return null;
        }
    }

    public function get_announcement_ended_by_id($id, $offset) {
        try {
            $id = $this->db->quote($id);
            $query = 'SELECT * FROM announcement WHERE state = "ended" AND user_id=' . $id . ' ORDER BY id DESC LIMIT 6 OFFSET ' . $offset;
            $announcements = $this->db->query($query)->fetchAll(PDO::FETCH_CLASS, "Announcement");
            return $announcements;
        } catch (PDOException $ex) {
            return null;
        }
    }

    public function get_all_announcement() {
        try {
            $query = 'SELECT * FROM announcement ORDER BY id DESC LIMIT 8';
            return $result = $this->db->query($query)->fetchAll(PDO::FETCH_CLASS, "Announcement");
        } catch (PDOException $ex) {
            return null;
        }
    }

    public function get_recent_announcement($limit, $offset) {
        try {
            $query = 'SELECT * FROM announcement ORDER BY ID DESC LIMIT ' . $limit . ' OFFSET ' . $offset;
            $announcements = $this->db->query($query)->fetchAll(PDO::FETCH_CLASS, "Announcement");
            return $announcements;
        } catch (PDOException $ex) {
            return null;
        }
    }

    public function get_recent_announcements() {
        try {
            $query = 'SELECT * FROM announcement where state = "active" ORDER BY ID DESC LIMIT 12';
            $announcement = $this->db->query($query)->fetchAll(PDO::FETCH_CLASS, "Announcement");
            return $announcement;
        } catch (PDOException $ex) {
            return null;
        }
    }

    public function get_number_announcement_by_id($id) {
        try {
            $query = 'SELECT count(id) FROM announcement WHERE user_id = ' . $id . ' AND state = "active";';
            $result = $this->db->query($query)->fetchColumn();
            return $result;
        } catch (PDOException $ex) {
            return null;
        }
    }

    public function remove_announcement_try($announcement_id) {
        try {
            $current_id = $this->db->quote($_SESSION['id']);
            $announcement_id = $this->db->quote($announcement_id);
            $query = 'SELECT count(id) FROM announcement WHERE user_id = ' . $current_id . ' AND id = ' . $announcement_id;
            $result = $this->db->query($query)->fetchColumn();
            if ($result) {
                return true;
            }
            return false;
        } catch (PDOException $ex) {
            return -1;
        }
    }

    public function update_announcement($announcement_id) {
        try {
            $announcement_id = $this->db->quote($announcement_id);
            $query = 'UPDATE announcement SET state = "ended" WHERE id = ' . $announcement_id;
            $this->db->query($query);
        } catch (PDOException $ex) {
            return -1;
        }
    }

    public function get_number_ended_announcements_by_id($id) {
        try {
            $id = $this->db->quote($id);
            $query = 'SELECT count(id) FROM announcement WHERE user_id = ' . $id . ' AND state = "ended";';
            $result = $this->db->query($query)->fetchColumn();
            return $result;
        } catch (PDOException $ex) {
            return null;
        }
    }

    public function get_last_announcement_time($user_id) {
        try {
            $user_id = $this->db->quote($user_id);
            $query = 'SELECT * FROM announcement WHERE  user_id = ' . $user_id . ' AND TIMESTAMPDIFF(SECOND, time_announce, NOW()) < 300';
            $result = $this->db->query($query)->fetchAll(PDO::FETCH_CLASS, "Announcement");
            if (!count($result)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            return -1;
        }
    }

    public function find_category($category) {
        try {
            $category = $this->db->quote($category);
            $query = 'SELECT * from category where name =' . $category;
            $result = $this->db->query($query);
            return $result->rowCount();
        } catch (Exception $ex) {
            return -1;
        }
    }

    public function find_region($region) {
        try {
            $region = $this->db->quote($region);
            $query = 'SELECT * from region where name =' . $region;
            $result = $this->db->query($query);
            return $result->rowCount();
        } catch (Exception $ex) {
            return -1;
        }
    }

}
