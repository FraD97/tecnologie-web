
var emptySquareX = 300;
var emptySquareY = 300;
var gameIsStarted = false;
/*
 * La seguente funzione si occpua di mostrare correttamente l'immagine
 * selezionata per ogni tessera
 */

function setPositionImage(listPuzzles)
{
    var x = 0, y = 0;  
    for(var i = 0; i < listPuzzles.length; i++){    
        var string = x + "px " + y + "px";
        listPuzzles[i].className = "default";
        listPuzzles[i].style.left = -x + "px";
        listPuzzles[i].style.top = -y + "px";
        listPuzzles[i].style.backgroundPosition = string;
        listPuzzles[i].onmouseover = hasNextPuzzleEmpty;
        listPuzzles[i].onclick = move;
        listPuzzles[i].onmouseout = hasNextPuzzleEmpty;
        
        if(i % 4 === 3){
            x = 0;
            y -= 100;
        }
        else{
            x -= 100;
        }
    }   
}

function checkValues(x, y){
    if((x  === emptySquareX - 100 || x === emptySquareX + 100) && y === emptySquareY){
        return true;
    }
    else if((y  === emptySquareY - 100 || y === emptySquareY + 100) && x === emptySquareX){
        return true;
    }
    return false;
    
}

function hasNextPuzzleEmpty(){
    var thisX = parseInt(this.style.left);
    var thisY = parseInt(this.style.top);  
    if(checkValues(thisX, thisY)){ 
        this.addClassName("redborders");
    }
    else{
        this.removeClassName("redborders");
    }       
}

function shuffle(){
   gameIsStarted = true;
   var randomMoves = Math.floor((Math.random() * 100) + 50);
   for(var i = 0; i < randomMoves; i++){
       findCorrectPuzzleAndMove();
   }
}

function findCorrectPuzzleAndMove(){
    var listPuzzles = $$("#puzzlearea div");
    var i = Math.floor((Math.random() * listPuzzles.length));
 
    for(; i < listPuzzles.length; i++){
        var x = parseInt(listPuzzles[i].style.left);
        var y = parseInt(listPuzzles[i].style.top);
        if(checkValues(x, y)){
            moveOneBlock(listPuzzles[i]);
            break;
        }
    }
 
}

function displayImage() {
    var listPuzzles = $$("#puzzlearea div");
    setPositionImage(listPuzzles);
    
}

function move(){
    if(this.className === "default redborders"){
        moveOneBlock(this);
    }
    if(gameIsWon() && gameIsStarted){
        alert("Complimenti hai vinto!");
    }
}

function gameIsWon(){
    var listPuzzles = $$("#puzzlearea div");
    var x = 0, y = 0;
    for(var i = 0; i < listPuzzles.length; i++){
        if(x !== parseInt(listPuzzles[i].style.left) || y !== parseInt(listPuzzles[i].style.top)){
            return false;
        }
        if(i % 4 === 3){
            x = 0;
            y += 100;
        }
        else{
            x += 100;
        }
    }
    return true;
}

function moveOneBlock(puzzle){
    var newEmptySquareX = parseInt(puzzle.style.left);
    var newEmptySquareY = parseInt(puzzle.style.top);
        if(emptySquareY === newEmptySquareY){
            puzzle.style.left = emptySquareX + "px";
            emptySquareX = newEmptySquareX;
        }
        else{
            puzzle.style.top = emptySquareY + "px";
            emptySquareY = newEmptySquareY;
    }
  
}

function setButton()
{
    var shuffleBtn = $("controls");
    shuffleBtn.onclick = shuffle;
}

window.onload = function(){
    displayImage();
    setButton();    
};

