<?php
    if($rows != null && $rows->rowCount() != 0){
        $i = 1;
?>      

<div id = "table_submit">
        <h4>All Films</h4>
          <table>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Year</th>
                </tr>
<?php        
        foreach($rows as $row){
            if($i % 2 == 1){
                $id_table = 'row_table_color';
            }
            else{
                $id_table = 'row_table_white';
            }
?>          
            <tr id = "<?= $id_table?>">
                <td><?= $i++ ?></td>
                <td><?= $row['name']?></td>
                <td><?= $row['year']?></td>
            </tr>
<?php        
        }
?>
        </table>  
    </div>
<?php
    }
    else{
?>
        <p>No film found for <?= $first_name." ".$last_name ?></p>
<?php    

    }
?>
