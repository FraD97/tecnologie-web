<?php
    include("top.html");
    include_once("functions.php");
    $rows = search_all_actors($first_name, $last_name); 
    
?>
    <h1>Results for <?= $first_name." ".$last_name?></h1>

<?php
    print_table($rows);
    include 'bottom.html';
?>
