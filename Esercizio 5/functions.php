<?php

    $first_name = $_GET['firstname'];
    $last_name = $_GET['lastname']; 
    
    
    function find_correct_id($first_name, $last_name, $db){
         $first_name = $first_name . '%';
         $first_name = $db->quote($first_name);
         $last_name = $db->quote($last_name);
         $query = "select min(X.id) as id
                   from(select *
                        from actors
                        where first_name like" . $first_name . "and last_name =" . $last_name . "and film_count = 
                                                                                    (select max(film_count)
                                                                                     from actors
                                                                                     where first_name like".$first_name.
                                                                                     "and last_name = ".$last_name."))X;";
         $id_row = $db->query($query);
         $id = $id_row->fetch(PDO::FETCH_ASSOC);
         return $id['id'];
    }
    
    
    function search_all_actors($first_name, $last_name){
        try{     
            $db = new PDO("mysql:dbname=imdb_small;host=localhost", "root", "");
            $id = find_correct_id($first_name, $last_name, $db);
            $id = $db->quote($id);
            $query = "select name, year from
                     (select * from actors WHERE id =".$id.")X JOIN roles ON X.id = 
                      roles.actor_id JOIN movies ON movies.id = roles.movie_id ORDER BY year DESC, name ASC";
            $rows = $db->query($query);
            return $rows;
            
        } catch (PDOException $ex) {
            ?>
            <p>Sorry, a database error occurred.</p>
            <?php
            return NULL;
        }
    }
    
    function search_kevin($first_name, $last_name){
        try{
            $db = new PDO("mysql:dbname=imdb_small;host=localhost", "root", "");
            $id = find_correct_id($first_name, $last_name, $db);
            $id = $db->quote($id);
            $query = "SELECT A1.MOVIE_NAME as name, A1.MOVIE_YEAR as year
                      FROM(
                        SELECT A.id as ACTOR_ID, M.id as MOVIE_ID, M.name as MOVIE_NAME, M.year as MOVIE_YEAR
                        FROM actors A join roles R on A.id = R.actor_id  JOIN movies M on R.movie_id = M.id
                        WHERE first_name = 'kevin' and last_name = 'Bacon')A1 
                        JOIN(
                        SELECT M.id as MOVIE_ID
                        FROM actors A join roles R on A.id = R.actor_id  JOIN movies M on R.movie_id = M.id
                        WHERE A.id =".$id.")A2
                        ON A1.MOVIE_ID = A2.MOVIE_ID ORDER BY year DESC, name ASC";
            $rows = $db->query($query);
            return $rows;
        } catch (PDOException $ex) {
            ?>
            <p>Sorry, a database error occurred.</p>
            <?php
            return NULL;
        }      
    }


    function print_table($rows){
        if($rows != null && $rows->rowCount() != 0){
            $i = 1;
    ?>      
        <div id = "table_submit">
                <h4>All Films</h4>
                  <table>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Year</th>
                        </tr>
        <?php        
                foreach($rows as $row){
        ?>          
                    <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $row['name']?></td>
                        <td><?= $row['year']?></td>
                    </tr>
        <?php        
                }
        ?>
                </table>  
            </div>
        <?php
            }
            else{
        ?>
                <p>No film found for <?= $first_name." ".$last_name ?></p>
        <?php    

            }

        }